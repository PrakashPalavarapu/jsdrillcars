module.exports = function GetCarYears(inventory) {
    let input_type = typeof inventory;
    if (inventory.length === 0) {
        return `the inventory is empty please check`
    } else if (input_type !== "object") {
        return `please check the input type`;
    }
    let car_models = [];
    for (let car = 0; car < inventory.length; car++) {
        car_models.push(inventory[car]["car_year"]);
    }
    return car_models.sort();
}

/*
Handels the following test cases
1. given wrong type of input
2. given empty input
*/

