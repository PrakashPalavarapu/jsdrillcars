module.exports = function GetBMWandAudi(inventory) {
    let input_type = typeof inventory;
    if (inventory.length === 0) {
        return `the inventory is empty please check`
    } else if (input_type !== "object") {
        return `please check the input type`;
    }
    let BMWAndAudi = [];
    for (let car = 0; car < inventory.length; car++) {
        if (inventory[car]["car_make"] === "Audi" || inventory[car]["car_make"] === "BMW") {
            BMWAndAudi.push(inventory[car]);
        }
    }
    return BMWAndAudi;
}

/*
Handels the following test cases
1. given wrong type of input
2. given empty input
*/