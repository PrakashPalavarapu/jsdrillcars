// information for a car with an id of 33 on his lot

// The aurguments that should be passed are inventory and id number

function GetCarByID(inventory = [], id = 33) {
    let input_type = typeof inventory;
    if (inventory.length === 0) {
        return `the inventory is empty please check`
    } else if (input_type !== "object") {
        return `please check the input type`;
    }
    let check = 1;
    for (let car = 0; car < inventory.length; car++) {
        if (inventory[car]["id"] == id) {
            check = 0;
            return `Car ${id} is a ${inventory[car]["car_year"]} ${inventory[car]["car_make"]} ${inventory[car]["car_model"]}`;
        }
    }
    if (check) {
        return `car with id ${id} is not listed in the in the inventory`;
    }
}

module.exports = GetCarByID;


/*

handels the following round cases
1. passing id which is not in the inventory
2. passing wrong type of input
3. passing empty inventory

*/