// returns the last car in the inventory

function LastCar(inventory) {
    let input_type = typeof inventory;
    if (inventory.length === 0) {
        return `the inventory is empty please check`
    } else if (input_type !== "object") {
        return `please check the input type`;
    }
    let inventory_size = inventory.length;
    result = inventory[inventory_size - 1]
    return `Last car is a ${result["car_make"]} ${result["car_model"]}`;
}

module.exports = LastCar;

/*
Handels the following test cases
1. given wrong tyoe of input
2. given empty input
*/