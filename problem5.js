const GetCarYear = require("./problem4");

module.exports = function CarsOlderThan2000(inventory) {
    if (inventory.length === 0) {
        return `the inventory is empty please check`
    } else if (input_type !== "object") {
        return `please check the input type`;
    }
    const all_years = GetCarYear(inventory);
    let year_2000 = [];
    for (let car = 0; car < all_years.length; car++) {
        if (all_years[car] < 2000) {
            year_2000.push(all_years[car]);
        }
    }
    return year_2000.length;
}

/*
Handels the following test cases
1. given wrong type of input
2. given empty input
*/