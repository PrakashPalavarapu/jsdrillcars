# Intro
Completed JS-Drill Cars
Cars inventory data saved in file data.js
The solutions were included in problem{1..6}.js files
and the test files are in test folders with the names testProblem{1..6}.js

## Problem1 : 
created a function that returns car details of car having given id. Also returns an acknowledgement if there is no id present in the inventory

## problem2 :
Returns the last car in the inventory

## Problem3 :
Returns the sorted car list

## Problem4 :
returns car years of all cars

## Problem5 :
Returns the number of cars that are older than year 2000

## Problem6 :
Returns all the details of cars of make BMW and Audi

# Error Handling
For *problem1*

handels the following test cases
1. passing id which is not in the inventory
2. passing wrong type of input
3. passing empty inventory

For *Problem 2-6*
handels the following test cases
1. passing wrong type of input
2. passing empty inventory


# What I have Learned
1. Writing functions
2. Exporting and importing files
3. giving default parameters while function declaration
4. functions with and withour return
5. iterating through object and list
5. indexing objects and list
6. Error Handling and thinking of all test cases

